# Phone Simulator for modem testing

Copyright (C) 2008-2009  Intel Corporation. All rights reserved.


Compilation and installation
----------------------------

In order to compile phone simulator you need following software packages:
 - GCC compiler
 - Qt libraries

To configure run:

```
mkdir build && cd build
cmake -GNinja ..
```

CMake automatically detects the location of the dependencies.
To compile and install run:
```
ninja && ninja install
```
