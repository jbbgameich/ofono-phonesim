cmake_minimum_required(VERSION 2.8.12)
project(phonesim)
set(PHONESIM_VERSION 1.21)

################# Disallow in-source build #################

if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
   message(FATAL_ERROR "This application requires an out of source build. Please create a separate build directory.")
endif()

include(FeatureSummary)
include(GNUInstallDirs)

find_package(Qt5 "5.10.0" REQUIRED NO_MODULE COMPONENTS Core Widgets Qml Network DBus)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

################# Enable C++14 features for clang and gcc #################

set(CMAKE_CXX_STANDARD 14)

################# build and install #################
add_subdirectory(src)

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
